#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <iomanip>
#include <random>
#include <chrono>
#include <fstream>
#include <string>
#include <bits/stdc++.h>

using namespace std;

// Primeiramente, leia o arquivo "README.txt"

// Função responsável por determinar um padrão para imprimir automaticamente um vector de double no terminal
// Para imprimir um vector de valores double A, basta realizar o comando: cout << A;
std::ostream& operator<<(std::ostream& os, const std::vector<double> &input){
    for(auto const& i: input){
        os << i << "\n";
    }
    return os;
}

// Função responsável por determinar um padrão para imprimir automaticamente um vector de valores inteiros no terminal
// Para imprimir um vector de valores inteiros A, basta realizar o comando: cout << A;
std::ostream& operator<<(std::ostream& os, const std::vector<int> &input){
    for(auto const& i: input){
        os << i << "\n";
    }
    return os;
}

// Função responsável por determinar um padrão para imprimir automaticamente uma matriz de double,
// implementada usando vector, isto é, um vector bidimensional de double
std::ostream& operator<<(std::ostream& os, const std::vector<vector<double>> &input){
    for(auto const& i: input){
        for(auto const& j: i)
            os << std::setprecision(5) << j << " ";
        os << "\n";
    }
    return os;
}

// Função para sobrescrever o operador de mutliplicação para o caso da mutiplicação entre dois vectors bidiomensionais,
// o que corresponde a uma mutiplicação de matrizes.
// Sendo A e B dois vectos bidiomensionais, a mutiplicação deles poderá ser feita simplesmente usando A*B.
vector<vector<double>> operator* (vector<vector<double>> a, vector<vector<double>> b){

    vector<vector<double>> product(a.size(), vector<double>(b[0].size(), 0)); // Initializing result vector with zeros

    for(int i=0; i<a.size(); ++i)
        for(int j=0; j<b[0].size(); ++j)
            for(int k=0; k<b.size(); ++k) {
                product[i][j]+=a[i][k]*b[k][j];
            } 
    return product;
}

// Sobrescrenvendo o operador de multiplicação para definir a mutliplicação de um vector bidimensional por um
// vector unidimensional, ambos formados por valores inteiros (double).
vector<double> operator* (vector<vector<double>> a, vector<double> b){
    
    vector<double> product (a.size(), 0);

    for(int i=0; i<a.size(); ++i)
        for(int j=0; j<1; ++j)
            for(int k=0; k<b.size(); ++k) {
                product[i]+=a[i][k]*b[k];
            }

    return product;  
}

// Função auxiliar responsável por retornar a matriz transposta dada a partir de uma matriz fornecida como argumento
// da função transpose.
vector<vector<double>> transpose(vector<vector<double>>& A){

    // Inicializando a matriz transposta com valores nulos
    vector<vector<double>> A_transpose(A[0].size(), vector<double>(A.size(), 0));

    #pragma omp parallel for // Este comando tenta realizar paralelamente o processamento do laço for abaixo
    for(int i = 0; i < A.size(); i++){
        for(int j = 0; j < A[0].size(); j ++){
            A_transpose[j][i] = A[i][j];
        }
    }

    return A_transpose;
}

// Função auxiliar para gerar números aleatórios a partir de uma variável aleatória com distribuição uniforme no intervalo
// de 0 a 1000, sendo os valores fornecidos do tipo double.
double doubleRand(){
    random_device rd;
    mt19937 gen(rd());
    std::uniform_real_distribution<double> distribution(0.0,1000.0);

    return distribution(gen);
}

// Função auxiliar para checar se uma dada matriz possui um valor NaN
// Foi apenas utilizada durante o desenvolvimento do EP, para identificar erros
bool checkNaN(vector<vector<double>> A){
    bool result = 0;
    for(int i = 0; i < A.size(); i++)
        #pragma omp parallel for
        for(int j = 0; j < A[0].size(); j++)
            if(isnan(A[i][j])){
                result = 1;
            }
    return result;
}

// Função auxiliar para checar se uma dada matriz possui um valor Inf (infinito)
// Foi apenas utilizada durante o desenvolvimento do EP, para identificar erros
bool checkInf(vector<vector<double>> A){
    bool result = 0;
    for(int i = 0; i < A.size(); i++)
        #pragma omp parallel for
        for(int j = 0; j < A[0].size(); j++)
            if(isinf(A[i][j])){
                result = 1;
            }
    return result;
}

// Função para determinação numérica dos valores do sen (s) e do cosseno (c) utilizados na Rotação de Givens.
// Esta função retorna simultaneamente os valores de c e s, através de um vector contendo os dois valores.
// Argumentos da função: w1 é w(i,k), w2 é w(j,k).
vector<double> calculateCS(double w1, double w2){
    
    double tau, c, s;
    std::vector<double> cs;
    tau = c = s = 0.0;

    // Realizando a comparação dos valores abolutos de w1 e w2 com a função fabs(), que fornece o valor absoluto como
    // um double.
    if(fabs(w1) > fabs(w2)){
        tau = - w2/w1;
        c = (1/sqrt(1 + tau*tau));
        s = c * tau;    
    }   

    else{
        tau = - w1/w2;
        s = (1/sqrt(1 + tau*tau));
        c = s * tau;
    }
    
    cs.push_back(c);
    cs.push_back(s);
    return cs;
}

// Função para a realização da Rotação de Givens aplicada sobre uma matriz W
// Conforme descrito no enunciado, em alguns casos faz sentido iniciar a rotação de givens a partir de uma certa coluna
// caso as primeiras linhas tenham apenas valores nulos
void rot_givens(vector<vector<double>>& W, int n, int m, int i, int j, double c, double s, int k=0){
    double aux;

    #pragma omp parallel for
    for(int r = k; r < m; r++){
        aux = c * W[i][r] - s * W[j][r];
        W[j][r] = s * W[i][r] + c * W[j][r];
        W[i][r] = aux;
    }
}

// Função para resolver um sistema do tipo W * X = B, onde W é uma matriz (n x m), X é um vetor (m x 1) e
// um vetor B (n x 1). A resolução é feita decompondo as matrizes em matrizes Q e R, através de sucessivas
// Rotações de Givens. A matriz W não é necessariamente quadrada, isto é, m pode não ser igual a n.
void QR_solver(vector<vector<double>>& W, int n, int m, vector<vector<double>>& b, vector<double>& x){
    
    for(int k = 0; k < m; k++){
        for(int j = n - 1; j > k; j--){
            int i = j - 1;

            if(W[j][k] != 0){

                auto cs = calculateCS(W[i][k], W[j][k]);

                // Aplicando a Rotação de Givens até tornar W uma matriz triangular superior.
                rot_givens(W, n, m, i, j, cs[0], cs[1], k);

                // Aplicando a mesma Rotação aplicada em W, mas no vetor B.
                rot_givens(b, b.size(), 1 ,i, j, cs[0], cs[1], 0);
            }
        }
    }

    // Calculando os valores finais do vetor X
    for(int k = m-1; k >= 0; k--){
        double sum = 0;
        
        for(int j = k; j < m; j++){
            sum += W[k][j]*x[j];
        }
        if(W[k][k] != 0.0)
            x[k] = (b[k][0] - sum)/W[k][k];
    }
}

// Função para resolver um sistema do tipo W * X = B, onde W é uma matriz (n x m), X é um vetor (m x 1) e
// um vetor B (n x 1). A resolução é feita decompondo as matrizes em matrizes Q e R, através de sucessivas
// Rotações de Givens.
void QR_solver_multiple(vector<vector<double>>& W, int n, int m, vector<vector<double>>& A, vector<vector<double>>& h, int p){  
    
    for(int k = 0; k < p; k++){
        for(int j = n - 1; j > k; j--){
            int i = j - 1;
            if(W[j][k] != 0 || W[i][k] !=0 ){
                auto cs = calculateCS(W[i][k], W[j][k]);
                rot_givens(W, n, p, i, j, cs[0], cs[1]);
                rot_givens(A, A.size(), A[0].size() ,i, j, cs[0], cs[1], 0);
            }
        }
    }
    
    for(int k = p - 1; k >= 0; k--){
        for(int j = 0; j < m; j++){
            double sum = 0;
            
            // #pragma omp parallel for
            for(int i = k; i < p; i++){
                sum += (W[k][i]*h[i][j]);
            }
            if(W[k][k] != 0.0)
                h[k][j] = (A[k][j] - sum)/W[k][k]; 
        }
    }
}

// Função para normalização das colunas de uma matriz W.
// Esta função é utilizada para a fatoração por matrizes não-negativas, isto é, a função alternating_least_squares().
void normalizeCollumns(vector<vector<double>>& W){
    // #pragma omp parallel for
    for(int j = 0; j < W[0].size(); j++){
        double sum_of_squares = 0.0;

        for(int i = 0; i < W.size(); i++){
            sum_of_squares += W[i][j] * W[i][j];
        }

        if(sum_of_squares != 0.0){
            for(int i = 0; i < W.size(); i++){
                W[i][j] = W[i][j] / sqrt(sum_of_squares);
            }    
        }
    }
}

// Esta função é utilizada para a fatoração por matrizes não-negativas, isto é, a função alternating_least_squares().
// Ela fornece a norma do erro definido por ||A - WH||, que é utilizado na verificação da convergência do algoritmo
// da fatoração por matrizes não-negativas.
double calculateError(vector<vector<double>> A, vector<vector<double>> H, vector<vector<double>> W){
    auto W_H = W*H;
    double sum = 0.0;

    for(int i = 0; i < W.size(); i++){
        double partial_sum = 0.0;

        for(int j = 0; j < H[0].size(); j++){
            partial_sum += (A[i][j] - W_H[i][j]) * (A[i][j] - W_H[i][j]);
        }
        sum += partial_sum;
    }
    return sum;
}

// Esta função implementa a fatoração WH de uma matriz A. Sendo W uma matriz de dimensão n x p, H uma matriz de dimensão
// p x m, A é uma matriz de dimensão n x m.
// Os valores epsilon e itmax são, por padrão, os valores fornecidos pelo enunciado do Exercício Programa, para quando
// esses parâmetros não são fornecidos à função.
void alternating_least_squares(vector<vector<double>>& A, int n, int m, int p, vector<vector<double>>& W, vector<vector<double>>& H, double epsilon = 1e-05, int itmax = 100){
    
    // Criando uma cópia da matriz A original. A cópia será utilizada no algoritmo a seguir
    vector<vector<double>> A_copy;
    
    // Inicializando a matriz W com valores aleatórios uniformemente distribuídos
    for(int i = 0; i < n; i++){
        vector<double> aux(p, 0);

        // #pragma omp parallel for
        for(int j = 0; j < p; j++){
            aux[j] = doubleRand();
        }
        W.push_back(aux);
    }

    // Inicializando as variáveis para inspecionar a convergência e quantidade de iterações do algoritmo
    int iteration = 0;
    double last_error, error;
    last_error = error = 0.0;

    // Início do algoritmo
    while(iteration < itmax){
        // Como A_copy é alterada durante cada passo do algoritmo, ela recebe novamente seu valor inicial, guardado na
        // matriz A original.
        A_copy = A;

        // Normalização por colunas da matriz W
        normalizeCollumns(W);

        // Trocando elementos negativos da matriz H por valores nulos
        #pragma omp parallel for
        for(int i = 0; i < H.size(); i++){
            for(int j = 0; j < H[0].size(); j++)
                H[i][j] = 0.0;
        }
        
        // Resolução dos múltiplos sistemas
        QR_solver_multiple(W, n, m, A_copy, H, p);
        
        // Trocando elementos negativos da matriz H por valores nulos
        #pragma omp parallel for
        for(int i = 0; i < H.size(); i++){
            for(int j = 0; j < H[0].size(); j++){
                if(H[i][j] < 0)
                    H[i][j] = 0;
            }
        }

        // Criando as matrizes A transposta e H transposta
        auto A_transpose = transpose(A);
        auto H_transpose = transpose(H);

        // Inicializando a matriz transposta de W com valores nulos
        vector<vector<double>> W_transpose(W[0].size(), vector<double>(W.size(), 0)); 

        // Resolvendo o sistema H^t * W^t = A^t
        QR_solver_multiple(H_transpose, m, n, A_transpose, W_transpose, p);
        
        // Transpondo a matriz transposta de W para obter a W verdadeira
        W = transpose(W_transpose);

        // Trocando elementos negativos da matriz W por valores nulos        
        #pragma omp parallel for
        for(int i = 0; i < W.size(); i++){
            for(int j = 0; j < W[0].size(); j++){
                if(W[i][j] < 0.0)
                    W[i][j] = 0.0;
            }
        }

        iteration++;

        // Calculando o erro definido como a norma: ||A - W*H||
        error = calculateError(A, H, W);
        
        // Verificando a convergência do algoritmo em relação a epsilon
        if(abs(last_error - error) < epsilon){
            break;
        }

        // Caso contrário, atualiza-se o valor do último erro do algoritmo
        else
            last_error = error;
    }
}

// Função para leitura de um arquivo com os dados para o treinamento do classificador para um certo dígito recebido
// como argumento da função.
// Esta função também recebe um parâmetro que informa quantos dígitos serão utilizados para o treinamento do classificador.
vector<vector<double>> readTrainingData(const string name, const int ndig_treino){
    string aux;
    int size = 0;

    fstream file_size;
    file_size.open("./dados_mnist/" + name);

    // Joga erro caso não seja possível abrir o arquivo
    if(!file_size.is_open())
        throw std::runtime_error("File not found");
    
    // Primeiramente é feita uma leitura prévia do arquivo completo para determinar a quantidade de colunas presentes nele.
    while(1){
        file_size >> aux;

        // Caso ocorra algum erro na leitura, ou o fim do arquivo seja atingido durante a leitura, encerra-se a leitura.
        if(!file_size.good())
            break;

        size++;
    }
    
    file_size.close();

    // O arquivo é aberto novamente, com a quantidade de colunas já conhecidas para que seja feita realmente a leitura
    // e carregamento dos dados de uma quantidade ndig_treino de imagens para o treinamento do classificador de digitos.
    fstream file;
    file.open("./dados_mnist/" + name);

    // Joga erro caso não seja possível abrir o arquivo
    if(!file.is_open())
        throw std::runtime_error("File not found");

    int columns = size/784;
    vector<vector<double>> data;

    for(int j = 0; j < 784; j++){
        vector<double> line(ndig_treino, 0);
        for(int i = 0; i < ndig_treino; i++){
            file >> line[i];
        }
        for(int i = ndig_treino; i < columns; i++){
            file >> aux;
        }
        data.push_back(line);        

        // Caso ocorra algum erro na leitura, ou o fim do arquivo seja atingido durante a leitura, encerra-se a leitura.
        if(!file.good())
            break;        
    }

    file.close();

    for(int i = 0; i < data.size(); i++){
        for(int j = 0; j < data[0].size(); j++){
            data[i][j] = data[i][j] / 255;
        }
    }
    return data;
}

// Função para leitura das imagens disponíveis no arquivo "/dados_mnist/test_images.txt".
// Por padrão, caso não seja informado um número de imagens para serem lidas, a função lerá todas as 10000 imagens.
vector<vector<double>> readTestImages(int ntest = 10000){
    string aux;
    int size = 0;

    fstream file;
    file.open("./dados_mnist/test_images.txt");

    // Joga um erro caso não seja possível abrir o arquivo com as imagens
    if(!file.is_open())
        throw std::runtime_error("File not found");

    vector<vector<double>> data;

    for(int j = 0; j < 784; j++){
        vector<double> line(ntest, 0);

        // Leitura dos primeiros ntest valores de uma linha do arquivo com as imagens.
        // Valores lidos para a linha são adicionados num vector chamado line.
        for(int i = 0; i < ntest; i++){
            file >> line[i];
        }

        // Demais colunas para uma linha do arquivo são ignoradas.
        for(int i = ntest; i < 10000; i++){
            file >> aux;
        }

        // Vector line é adicionado ao vector bidimensional, contendo todos os valores úteis presentes em uma linha do
        // arquivo que contém as imagens.
        data.push_back(line);

        // Caso ocorra algum erro na leitura, ou o fim do arquivo seja atingido durante a leitura, encerra-se a leitura.
        if(!file.good())
            break;        
    }

    // Fechando o arquivo que estava sendo lido
    file.close();

    // Normalização dos valores lidos para as imagens, isto é, valores de 0 a 255, já que a imagem é em preto e branco
    // Sendo assim, todos valores lidos são divididos por 255.
    for(int i = 0; i < data.size(); i++){
        for(int j = 0; j < data[0].size(); j++){
            data[i][j] = data[i][j] / 255;
        }
    }

    // Retornando a matriz (vector bidimensional) com os dados úteis lidos para uma quantidade de imagens solicitada
    return data;
}

// Função para classificação de dígitos presentes em imagens.
// Esta função recebe um vector tridimensional contendo as 10 matrizes que foram treinadas para reconhecimento dos 10
// dígitos possíveis.
void classifyDigits(vector<vector<double>>& images, vector<vector<vector<double>>>& trainedData, vector<double>& digit_error, vector<int>& digit){
    
    vector<vector<double>> H(trainedData[0][0].size(), vector<double>(images[0].size(), 0));
    vector<vector<double>> error;
    auto W_copy = trainedData;
    auto A_copy = images;
    for(int i = 0; i < 10; i++){

        //Resolvendo sistema para cada uma das 10 matrizes obtidas após os treinamentos
        QR_solver_multiple(W_copy[i], images.size(), images[0].size(), A_copy,  H, W_copy[0][0].size());
        
        auto WH = trainedData[i]*H;
        vector<double> image_error;

        //Calculando erro de cada imagem
        for(int j = 0; j < images[0].size(); j++){
            
            double sum = 0.0;
            for(int k = 0; k < images.size(); k++){
                sum += (images[k][j] - WH[k][j]) * (images[k][j] - WH[k][j]);
            }
            sum = sqrt(sum);

            // Adiciona os erros calculados para uma das imagens 
            image_error.push_back(sum);
        }
        error.push_back(image_error);
    }

    // Escolhendo o menor erro para cada um dos dígitos de entrada
    for(int j = 0; j < error[0].size(); j++){
        double max_error = error[0][j];
        int index = 0;
        for(int i = 1; i < 10; i++){
            if(max_error > error[i][j]){
                max_error = error[i][j];
                index = i;
            }
        }

        // Adicionando o erro mínimo e o índice do vetor dígito fornecido pelas matrizes treinadas para cada um dos dígitos
        // de entrada.
        digit_error.push_back(max_error);
        digit.push_back(index);
    }
}