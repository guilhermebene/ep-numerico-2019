MAP3121 - Exercıcio Programa 1
Fatoração de Matrizes para Sistemas de Classificação de Machine Learning
Autores: Guilherme Palmeira Kisahleitner - NUSP: 9837604
         Davi Braga de Vasconcelos Guedes - NUSP: 10335382

***********************************************************************
                    Detalhes sobre o desenvolvimento
***********************************************************************

1) Todos os programas escritos para este exercício foram feitos utilizando C++ 11.
2) Para a compilação e testes, foi utilizado o sistema operacional Linux Mint 19 Cinnamon.
3) Não foi utilizada nenhuma biblioteca que já não seja instalada por padrão com o g++ ou o sistema operacional.
4) O Computador utilizado para os testes possui 8 GB de memória RAM, entretanto, durante os testes verificou-se que
normalmente não é necessário utilizar mais do que 4.6 GB de memória RAM para execução dos programas deste Exercício Programa.

***********************************************************************
                            Compilação
***********************************************************************

1) Para realizar a compilação, recomenda-se utilizar o mesmo comando utilizado durante os testes:
g++ -pg <nome-do-arquivo.cpp> -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
O valor <nome-do-arquivo.cpp> deve ser substituido pelo nome do arquivo com extensão cpp a ser compilado. Abaixo estão alguns exemplos:
g++ -pg tarefa1a.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
g++ -pg tarefa1b.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
g++ -pg tarefa1c.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
g++ -pg tarefa1d.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
g++ -pg tarefa2.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4
g++ -pg tarefa3.cpp -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp -floop-parallelize-all -ftree-parallelize-loops=4

2) Caso o comando acima não funcione, recomenda-se utilizar:
g++ -pg $1 -O2 -o programa -lm -mcmodel=medium -std=c++11 -fopenmp
O valor $1 deve ser substituido pelo nome do arquivo com extensão cpp a ser compilado, conforme explicado acima.

3) Como terceira opção, é possível compilar com o comando: 
g++ -pg $1 -O2 -o programa -lm -mcmodel=medium -std=c++11
O valor $1 deve ser substituido pelo nome do arquivo com extensão cpp a ser compilado, conforme explicado acima.
Contudo, nesse caso será necessário retirar todos os comandos #pragma omp parallel for do arquivo functions.cpp.

***********************************************************************
                            Execução
***********************************************************************

1) Para executar o programa compilado, basta executar o comando seguinte no terminal:
./programa
2) Para executar o programa e guardar as saídas em um arquivo de texto, recomenda-se utilizar:
./programa > <arquivo.txt>
Onde <arquivo.txt> é o nome do arquivo escolhido para a saída de dados.
Vale notar que o comando descrito irá sobrescrever tudo que estiver previamente escrito nesse arquivo de texto para saída dos dados.