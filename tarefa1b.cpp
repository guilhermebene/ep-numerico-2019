#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

int main(){
    vector<vector<double>> W;

    // Inicializando o vetor b2 com valores unitários
    vector<vector<double>> b2(20, vector<double>(1,1));

    // Alterando os elmentos de b2 conforme definido no enunciado
    for(int i = 0; i < 20; i++){
        b2[i][0] = i + 1;
    }
    
    // Inserindo os valores em W a partir de um vector auxiliar, que é inserido como uma linha da matriz W
    for(int i = 0; i < 20; i ++){
        vector<double> aux(17, 0.0);
        for(int j = 0; j < 17; j++){
                if(abs(i - j) <= 4){
                    aux[j] = 1/(double(i+1 + j+1 - 1));
                }       

                if(abs(i - j) > 4){
                    aux[j] = 0;
                }
        }
        W.push_back(aux);
    }

    // Imprimindo valores inseridos na matriz W
    cout << "Valores inseridos na matriz W:\n" <<  W;

    // Imprimindo valores inseridos no vetor b2
    // Aqui chama-se o vetor b2 de B para manter a notação do enunciado
    cout << "\nValores inseridos no vetor B:\n" <<  b2; 

    vector<double> x(17,0.0);
    QR_solver(W, 20, 17, b2, x);

    // Imprimindo valores finais do vetor X após a decomposição das matrizes
    cout << "\nValores resultantes no vetor X:\n" <<  x;

    return 0;
}