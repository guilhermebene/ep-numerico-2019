#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

// Primeiro teste para solução de vários sistemas simultâneos

int main(){
    vector<vector<double>> W;
    
    // Populando a matriz W com os valores segundo o enunciado
    for(int i = 0; i < 64; i++){
        vector<double> aux(64, 0.0);
        for(int p = 0; p < 64; p++){
                if(i == p){
                    aux[p] = 2;
                }

                if(abs(i-p) == 1){
                    aux[p] = 1;
                }

                if(abs(i - p) > 1){
                    aux[p] = 0;
                }
        }
        W.push_back(aux);
    }

    // Populando as matrized H e A com 64 linhas e 3 colunas, sendo todas as posições inicialmente nulas
    vector<vector<double>> H(64, vector<double>(3, 0));
    vector<vector<double>> A(64, vector<double>(3,0));

    // Alterando os valores da matriz A segundo o enunciado
    for(int i = 0; i < 64; i++){
        A[i][0] = 1;
        A[i][1] = i+1;
        A[i][2] = 2*(i+1) - 1;  
    }

    // Imprimindo as matrizes W e A com seus valores iniciais
    cout << "Valores inseridos na matriz W:\n" <<  W;
    cout << "\nValores inseridos na matriz A:\n" <<  A; 

    // Realizando a solução dos sistemas simultâneos
    QR_solver_multiple(W, 64, 3, A, H, 64);

    // Imprimindo a matriz H final
    cout << "\nValores finais na matriz H:\n" <<  H; 

    return 0;
}