#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

int main(){
    int ndig_treino = 1000;
    int p = 10;
    string s;
    cout <<"**Start**\n";
    cout << "Insert p: ";
    cin >> p;
    cout <<"\nInsert ndig_treino: ";
    cin >> ndig_treino;
    vector<vector<vector<double>>> trainedData;
    vector<vector<vector<double>>> digitData;

    // Laço para ler quantidade de digitos de treinamento para cada um dos algarismos possíveis e para o treinamento
    // dos classificadores de dígitos.
    for(int i = 0; i < 10; i++){
        string source;

        switch (i){
        case 0:
            source = "train_dig0.txt";
            break;
        
        case 1:
            source = "train_dig1.txt";
            break;
        

        case 2:
            source = "train_dig2.txt";
            break;

        case 3:
            source = "train_dig3.txt";
            break;

        case 4:
            source = "train_dig4.txt";
            break;

        case 5:
            source = "train_dig5.txt";
            break;

        case 6:
            source = "train_dig6.txt";
            break;
        
        case 7:
            source = "train_dig7.txt";
            break;

        case 8:
            source = "train_dig8.txt";
            break;

        case 9:
            source = "train_dig9.txt";
            break;
        }

        
        auto hi = readTrainingData(source, ndig_treino);
        digitData.push_back(hi);

        vector<vector<double>> H(p, vector<double>(ndig_treino,0));
        vector<vector<double>> W;

        // Treinamento do classificador para um dado algarismo
        alternating_least_squares(digitData[i], 784, ndig_treino, p, W, H);
        trainedData.push_back(W);
    }

    digitData.clear();
    vector<vector<double>> images = readTestImages();
    vector<double> digit_error;
    vector<int> digit;
    classifyDigits(images, trainedData, digit_error, digit);
    cout <<"Algarismos encontrados\n" << digit;
    cout<<"\nErros respectivos:\n" << digit_error;
    return 0;
}