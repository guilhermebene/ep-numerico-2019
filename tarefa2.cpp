#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

int main(){
    //Tarefa 2

    vector<vector<double>> A;

    // Inserindo valores na matriz A para realizar o teste do enunciado
    A.push_back({0.3, 0.6, 0});
    A.push_back({0.5, 0, 1});
    A.push_back({0.4, 0.8, 0});

    // Imprimindo a matriz A inicial
    cout <<"\nMatriz A inicial:\n" << A;

    // Criando as matrizes W e H
    // H já é fornecida com um tamanho inicial, enquanto W será definida dentro da função que realiza a decomposição
    vector<vector<double>> W;
    vector<vector<double>> H(2, vector<double>(3,0));

    // Realizando o processo para obter as matrizes W e H com os valores corretos que aproximam W*H de A
    alternating_least_squares(A, 3, 3, 2, W, H, 1e-10);

    // Imprimindo as matrizes W e H obtidas
    cout << "\nMatriz W obtida:\n" << W;
    cout << "\nMatriz H obtida:\n" << H;

    return 0;    
}