#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

int main(){
    //Tarefa 1 item a)
    vector<vector<double>> W; 

    // Inserindo os valores na matriz W, conforme o enunciado
    for(int i = 0; i < 64; i ++){
        vector<double> aux(64, 0.0);
        for(int j = 0; j < 64; j++){
                if(i == j){
                    aux[j] = 2;
                }

                if(abs(i-j) == 1){
                    aux[j] = 1;
                }

                if(abs(i - j) > 1){
                    aux[j] = 0;
                }
        }
        W.push_back(aux);
    }

    // Imprimindo os valores inseridos na matriz W
    cout << "Matriz W inserida:\n" << W;

    // Criando o vetor b com todos os elementos iguais a 1
    vector<vector<double>> b(64, vector<double>(1,1));

    vector<double> x(64,0);

    // Aplicando a decomposição em matrizes Q e R a partir de Rotações de Givens
    QR_solver(W, 64, 64, b, x);

    // Imprimindo o Vetor X obtido
    cout << "Vetor X obtido:\n" << x;
    
    return 0;
}