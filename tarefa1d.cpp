#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "functions.cpp"

using namespace std;

int main(){
    vector<vector<double>> W;

    // Populando a matriz W com os valores do enunciado
    for(int i = 0; i < 20; i ++){
        vector<double> aux(17, 0.0);
        for(int j = 0; j < 17; j++){
                if(abs(i - j) <= 4){
                    aux[j] = 1/(double(i+1 + j+1 - 1));
                }       

                if(abs(i - j) > 4){
                    aux[j] = 0;
                }
        }
        W.push_back(aux);
    }

    // Criando as matrizes H com dimensão 17x3 e A 20x3 e todas posições inicialmente nulas
    vector<vector<double>> H(17, vector<double>(3, 0.0));
    vector<vector<double>> A(20, vector<double>(3, 0.0));
    
    // Adicionando os valores na matriz A conforme o enunciado
    for(int i = 0; i < 20; i++){
        A[i][0] = 1;
        A[i][1] = i+1;
        A[i][2] = 2*(i+1) - 1;  
    }

    // Imprimindo as matrizes W e A com seus valores iniciais
    cout << "Valores inseridos na matriz W:\n" <<  W;
    cout << "\nValores inseridos na matriz A:\n" <<  A; 

    // Realizando a solução dos sistemas simultâneos
    QR_solver_multiple(W, 20, 3, A, H, 17);

    // Imprimindo a matriz H final
    cout << "\nValores finais na matriz H:\n" <<  H; 

    return 0;
}


